package com.sali.DemoFrameworks.controller;

import com.sali.DemoFrameworks.rest.FrameworkRest;
import com.sali.DemoFrameworks.rest.FrameworkVersionRest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MainControllerTest {

    @LocalServerPort
    private  int port;

    @Autowired
    private  TestRestTemplate restTemplate;

    private  String url;
    private  String uuid;
    private  FrameworkVersionRest frameworkVersionRest;
    private  FrameworkRest frameworkRest;
    private  HttpEntity<FrameworkVersionRest> requestSave;
    private  HttpHeaders headers;

    void pickUp() {
        url = "http://localhost:" + port + "/demoframeworks";
        uuid = UUID.randomUUID().toString();
        frameworkVersionRest = new FrameworkVersionRest();
        frameworkRest = new FrameworkRest();
        requestSave = new HttpEntity<>(frameworkVersionRest, headers);
        headers = new HttpHeaders();
    }


    @Test
    void testFindAll() {
        pickUp();
        ResponseEntity<FrameworkVersionRest[]> responseAll = restTemplate.getForEntity(url, FrameworkVersionRest[].class);
        assertNotNull(responseAll);
        assertTrue(responseAll.getStatusCode().is2xxSuccessful());
    }

    @Test
    void testSave() {
        pickUp();
        frameworkRest.setName("JS Framework");
        frameworkRest.setDescription("JS framework");
        frameworkRest.setUrl("https://jsframework.cz/");
        frameworkVersionRest.setUuid(uuid);
        frameworkVersionRest.setVersion("1.0");
        frameworkVersionRest.setDeprecationDate(LocalDate.now());
        frameworkVersionRest.setHypeLevel(10);
        frameworkVersionRest.setFramework(frameworkRest);

        headers.set("Content-Type", "application/json");

        ResponseEntity<Void> responseSave = restTemplate.postForEntity(url, requestSave, Void.class);

        assertNotNull(responseSave);
        assertTrue(responseSave.getStatusCode().is2xxSuccessful());

        ResponseEntity<FrameworkVersionRest> responseByUuid = restTemplate.getForEntity(url + "/" + uuid, FrameworkVersionRest.class);

        assertNotNull(responseByUuid);
        assertTrue(responseByUuid.getStatusCode().is2xxSuccessful());
    }

    @Test
    void testUpdate() {
        pickUp();
        testSave();

        frameworkVersionRest.setHypeLevel(3);

        ResponseEntity<Void> responsePut = restTemplate.exchange(url, HttpMethod.PUT, requestSave, Void.class);

        assertNotNull(responsePut);
        assertTrue(responsePut.getStatusCode().is2xxSuccessful());

        ResponseEntity<FrameworkVersionRest> responseUpdate = restTemplate.getForEntity(url + "/" + uuid, FrameworkVersionRest.class);

        assertNotNull(responseUpdate);
        assertNotNull(responseUpdate.getBody());
        assertEquals(3, responseUpdate.getBody().getHypeLevel());
    }

    @Test
    void testDelete() {
        pickUp();
        restTemplate.delete(url + "/" + uuid);
        ResponseEntity<FrameworkVersionRest> responseDelete = restTemplate.getForEntity(url + "/" + uuid, FrameworkVersionRest.class);

        assertNotNull(responseDelete);
        assertTrue(responseDelete.getStatusCode().is2xxSuccessful());
        assertNull(responseDelete.getBody());
    }
}