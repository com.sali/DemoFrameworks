package com.sali.DemoFrameworks.repository;

import com.sali.DemoFrameworks.domain.Framework;
import com.sali.DemoFrameworks.domain.FrameworkVersion;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DemoFrameworksRepositoryTest {

    @Autowired
    private DemoFrameworksRepository repository;
    private String uuid;

    @BeforeEach
    void setUp() {
        uuid = UUID.randomUUID().toString();
    }

    @Test
    void save() {
        FrameworkVersion frameworkVersion = new FrameworkVersion();
        Framework framework = new Framework();
        frameworkVersion.setUuid(uuid);
        frameworkVersion.setVersion("1.0");
        frameworkVersion.setHypeLevel(10);
        framework.setName("Best JS Framework");
        framework.setUrl("https://bestjsframework.com/");
        framework.setDescription("description");
        frameworkVersion.setFramework(framework);
        repository.save(frameworkVersion);

        frameworkVersion.setHypeLevel(3);

        FrameworkVersion retVal = repository.findByUuid(uuid);
        Assertions.assertNotNull(retVal);
        assertEquals(3, retVal.getHypeLevel());

    }

    @Test
    void findByUuid() {
        save();
        FrameworkVersion retVal = repository.findByUuid(uuid);
        Assertions.assertNotNull(retVal);
    }

    @Test
    void findAll() {
        List<FrameworkVersion> list = repository.findAll();
        Assertions.assertNotNull(list);
    }

    @Test
    void deleteByUuid() {
        save();
        repository.deleteByUuid(uuid);
        FrameworkVersion retVal = repository.findByUuid(uuid);
        Assertions.assertNull(retVal);
    }

}