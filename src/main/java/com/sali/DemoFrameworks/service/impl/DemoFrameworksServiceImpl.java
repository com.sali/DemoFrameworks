package com.sali.DemoFrameworks.service.impl;

import com.sali.DemoFrameworks.domain.FrameworkVersion;
import com.sali.DemoFrameworks.repository.DemoFrameworksRepository;
import com.sali.DemoFrameworks.service.DemoFrameworksService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
/**
 * @author salinger
 */

@Service
@Transactional
public class DemoFrameworksServiceImpl implements DemoFrameworksService {

    private DemoFrameworksRepository demoFrameworksRepository;

    public DemoFrameworksServiceImpl(DemoFrameworksRepository demoFrameworksRepository) {
        this.demoFrameworksRepository = demoFrameworksRepository;
    }

    @Override
    public List<FrameworkVersion> findAll() {
        return demoFrameworksRepository.findAll();
    }

    @Override
    public FrameworkVersion find(String uuid) {
        return demoFrameworksRepository.findByUuid(uuid);
    }

    @Override
    public void save(FrameworkVersion frameworkVersion) {
        if (frameworkVersion != null) {
            demoFrameworksRepository.save(frameworkVersion);
        }
    }

    @Override
    public void update(FrameworkVersion frameworkVersion) {
        if (frameworkVersion != null) {
            FrameworkVersion oldFrameworkVersion = demoFrameworksRepository.findByUuid(frameworkVersion.getUuid());
            if (oldFrameworkVersion != null) {
                frameworkVersion.setUuid(oldFrameworkVersion.getUuid());
                frameworkVersion.setId(oldFrameworkVersion.getId());
                frameworkVersion.getFramework().setId(oldFrameworkVersion.getId());
                demoFrameworksRepository.save(frameworkVersion);
            }
        }
    }

    @Override
    public void delete(String uuid) {
        demoFrameworksRepository.deleteByUuid(uuid);
    }
}
