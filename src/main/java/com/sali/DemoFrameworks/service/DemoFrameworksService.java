package com.sali.DemoFrameworks.service;

import com.sali.DemoFrameworks.domain.FrameworkVersion;

import java.util.List;
/**
 * @author salinger
 */

public interface DemoFrameworksService {

    List<FrameworkVersion> findAll();

    FrameworkVersion find(String uuid);
    void save(FrameworkVersion frameworkVersion);
    void update(FrameworkVersion frameworkVersion);
    void delete(String uuid);

}
