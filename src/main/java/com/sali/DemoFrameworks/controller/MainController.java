package com.sali.DemoFrameworks.controller;

import com.sali.DemoFrameworks.domain.FrameworkVersion;
import com.sali.DemoFrameworks.mapper.FrameworkMapper;
import com.sali.DemoFrameworks.mapper.FrameworkVersionMapper;
import com.sali.DemoFrameworks.rest.FrameworkVersionRest;
import com.sali.DemoFrameworks.service.DemoFrameworksService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author salinger
 */
@RestController
@RequestMapping("/demoframeworks")
public class MainController {

    private final DemoFrameworksService service;

    public MainController(DemoFrameworksService service) {
        this.service = service;
    }

    @GetMapping
    @Operation(summary = "getAll", description = "Return all frameworks")
    public List<FrameworkVersionRest> findAll() {
        List<FrameworkVersion> retVal = service.findAll();
        return FrameworkVersionMapper.INSTANCE.mapList(retVal);
    }

    @GetMapping("/{uuid}")
    @Operation(summary = "getByUuid", description = "Return framework by uuid")
    public FrameworkVersionRest findByUuid(@PathVariable String uuid) {
        FrameworkVersion retVal = service.find(uuid);
        return FrameworkVersionMapper.INSTANCE.map(retVal);
    }

    @PostMapping
    @Operation(summary = "save", description = "Save framework")
    public void save(@RequestBody @Valid FrameworkVersionRest frameworkVersionRest) {
        FrameworkVersion item = FrameworkVersionMapper.INSTANCE.map(frameworkVersionRest);
        service.save(item);
    }

    @PutMapping
    @Operation(summary = "update", description = "Update framework")
    public void update(@RequestBody @Valid FrameworkVersionRest frameworkVersionRest) {
        FrameworkVersion item = FrameworkVersionMapper.INSTANCE.map(frameworkVersionRest);
        service.update(item);
    }

    @DeleteMapping("/{uuid}")
    @Operation(summary = "delete", description = "Delete framework")
    public void delete(@PathVariable String uuid) {
        service.delete(uuid);
    }


}

