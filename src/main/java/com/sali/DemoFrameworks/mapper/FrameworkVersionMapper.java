package com.sali.DemoFrameworks.mapper;

import com.sali.DemoFrameworks.domain.FrameworkVersion;
import com.sali.DemoFrameworks.rest.FrameworkVersionRest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;
/**
 * @author salinger
 */

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FrameworkVersionMapper {

    FrameworkVersionMapper INSTANCE = Mappers.getMapper(FrameworkVersionMapper.class);

    List<FrameworkVersionRest> mapList(List<FrameworkVersion> retVal);

    FrameworkVersionRest map(FrameworkVersion retVal);

    FrameworkVersion map(FrameworkVersionRest frameworkRest);

}
