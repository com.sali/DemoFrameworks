package com.sali.DemoFrameworks.mapper;

import com.sali.DemoFrameworks.domain.Framework;
import com.sali.DemoFrameworks.domain.FrameworkVersion;
import com.sali.DemoFrameworks.rest.FrameworkRest;
import com.sali.DemoFrameworks.rest.FrameworkVersionRest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;
/**
 * @author salinger
 */

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FrameworkMapper {

    FrameworkMapper INSTANCE = Mappers.getMapper(FrameworkMapper.class);

    List<FrameworkRest> mapList(List<Framework> retVal);

    FrameworkRest map(Framework retVal);

    FrameworkVersion map(FrameworkRest frameworkRest);

}
