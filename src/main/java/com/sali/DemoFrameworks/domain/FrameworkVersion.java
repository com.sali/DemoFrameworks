package com.sali.DemoFrameworks.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
/**
 * @author salinger
 */

@Data
@Entity
@Table(name = "FrameworkVersion")
public class FrameworkVersion implements Serializable {
    private static final long serialVersionUID = 1L;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_framework")
    private Framework framework;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 36)
    private String uuid;
    private LocalDate deprecationDate;
    private int hypeLevel;
    @Column(length = 10)
    private String version;
}
