package com.sali.DemoFrameworks.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author salinger
 */

@Data
@Entity
@Table(name = "Framework")
public class Framework implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 36)
    private String uuid;
    @Column(length = 20)
    private String name;
    @Column(length = 255)
    private String url;
    private String description;

}
