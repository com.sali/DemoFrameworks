package com.sali.DemoFrameworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author salinger
 */

@SpringBootApplication
public class DemoFrameworksApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoFrameworksApplication.class, args);
	}

}
