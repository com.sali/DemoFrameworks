package com.sali.DemoFrameworks.repository;

import com.sali.DemoFrameworks.domain.FrameworkVersion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
/**
 * @author salinger
 */

public interface DemoFrameworksRepository extends JpaRepository<FrameworkVersion, Long> {

    public FrameworkVersion findByUuid(String uuid);

    public void deleteByUuid(String uuid);

}
