package com.sali.DemoFrameworks.rest;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Range;
import java.io.Serializable;
import java.time.LocalDate;

import java.io.Serializable;
/**
 * @author salinger
 */

@Data
public class FrameworkVersionRest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Schema(description = "UUID, ", required = true)
    @NotEmpty
    @Size(max = 36)
    @Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")
    private String uuid;

    @Schema(description = "Deprecation date of version, in format yyy-MM-dd", required = true, example = "2018-11-21")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalDate deprecationDate;
    @Schema(description = "Hype level", required = true)
    @NotNull
    @Range(min = 1l, max = 10l)
    private Integer hypeLevel;
    @Column(length = 10)
    private String version;
    private FrameworkRest framework;
}


