package com.sali.DemoFrameworks.rest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author salinger
 */
@Data
public class FrameworkRest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Schema(description = "UUID", required = true)
    @NotEmpty
    @Size(max = 36)
    @Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")
    private String uuid;
    @Schema(description = "Framework's name", required = true)
    @NotEmpty
    @Size(max = 20)
    private String name;
    @Schema(description = "Framework's URL", required = true)
    @NotEmpty
    @Size(max = 255)
    private String url;
    @Schema(description = "Description", required = true)
    @NotEmpty
    @Size(max = 255)
    private String description;

}
